#!/bin/bash
HTMLOUTPUT="/var/www/html/index.html"
TMPLFILE="/opt/iotproject/tmpl/index.tmpl.html"

INTERVAL=5


mosquitto_sub -h localhost -t '#' -F "%t %p" | while read topic payload; do
    if [[ "$topic" = "sensor/temp" ]]; then
        datetime=$(date +%H:%M:%S)
        cat $TMPLFILE | \
        sed "s/_ZEIT_/$datetime/g" | \
        sed "s/_TEMP_/$payload/g" >$HTMLOUTPUT
        sleep $INTERVAL

    else
        echo "NIGEFU"

    fi

done

#!/bin/bash
fname="/opt/iotproject/index.html"
tmplfile="/opt/iotproject/tmpl/index.tmpl.html"
datetime=$(date +%H:%M:%S)
INTERVAL=5
#directorytolist="/tmp"
directorytolist=$1
if [ "$#" = 0 ]; then
    directorytolist="/tmp"
    echo "default directory "
elif [ -d "$directorytolist" ]; then
    # OK
    echo "dyndir"
else
    echo "ERROR - not a dir"
fi
shift

counter=0

while [ "$counter" -lt 10 ] ; do
echo "****START*******"
echo "<html><body>" >$fname
echo "<h1>Mein Webserver</h1>" >>$fname
echo "<p>$datetime</p>" >>$fname
ls "$directorytolist" 2>/dev/null >>$fname
numfiles=$*
for htmlsnippet in $numfiles; do
    if [ -r "$htmlsnippet" ]; then
        cat "$htmlsnippet" >>$fname
    else
        echo "NIGEFU $htmlsnippet" >&2
    fi
done
echo "</body></html>" >>$fname
sleep $INTERVAL
counter=$(("$counter"+1))
done